#!/bin/bash

## 1 - Setup printers

## 2 - Setup bluetooth (optional)

## 3 - Setup virtualization tools

## 4 Setup Windows VM with devices passthrough

## 5 - Setup file sync between desktop and laptop

## 6 - Setup wacom one pen display (optional)

## 7 - Setup pinetime companion app (optional)

## 8 - Setup security rules

## 9 - Setup backup services
