#!/usr/bin/bash

# Installing packages
pacman -S xorg xorg-xinit arandr
pacman -S bspwm  # window manager
pacman -S sxhkd  # hot-key daemon
pacman -S polybar  # status bar
pacman -S rofi  # launcher
pacman -S picom  # compositor
pacman -S dunst  # notification manager
pacman -S feh  # wallpaper manager
pacman -S alacritty  # terminal emulator

pacman -S --needed devtools
pacman -S tmux neovim


# Configuring packages
# bspwm
# sxhkd
# polybar
# rofi
# picom
# dunst
