#!/usr/bin/bash

# https://wiki.archlinux.org/title/Wayland
# https://wiki.archlinux.org/title/River
# https://leon_plickat.srht.site/writing/river-setup-guide/article.html/#lets-get-started

# Installing packages
pacman -S wayland  # display server
pacman -S river  # compositing window manager
pacman -S waybar  # status bar
pacman -S fuzzel  # launcher
pacman -S mako  # notification manager
paru -S wlr-randr  # display configuration manager
paru -S wbg  # background image manager
pacman -S waylock  # screen locker

paru -S eww-wayland  # widgets
paru -S swhkd  # hot-key manager
paru -S wob  # progress bar for audio volume, brightness, ...

# Configuring packages
# river
# swhkd
# wlr-randr
# wbg
# waylock
# waybar
# fuzzel
# mako
