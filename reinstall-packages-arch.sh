#!/bin/bash

# lists all explicitly-installed arch packages
pacman -Qqe > ./pacman.list

# installs all arch packages listed in pacman.lst
# avoids reinstallation of already-up-to-date packages
pacman -S --needed --noconfirm $(cat ~/dotfiles/$HOSTNAME/pacman.list)
