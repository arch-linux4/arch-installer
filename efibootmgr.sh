#!/bin/bash

# initialize variables
cmdline=$(head -n 1 "/etc/kernel/cmdline")
efifiles=$(ls -1 "/boot/efi/EFI/Linux/")

# checks if user is logged in as root
username=$(whoami)
if [ "$username" != "root" ]; then
    echo "Serve il root, esco..."
    exit
fi

# prints simple version message
function version {
    echo "###################"
    echo "# Version 0.1.0-3 #"
    echo "###################"
}

# prints help menu
function helper {
    version
    echo "
    -v|--version
    -h|--help
    -c|--create-entries"
}

# creates UEFI boot entries
function create_entries {
    for l in $efifiles;
    do
        l_noefi=$(basename "$l" ".efi")
        efibootmgr --create --gpt --disk /dev/nvme0n1 --part 1 --label "$l_noefi" --loader 'efi\EFI\Linux\'"$l_noefi.efi""$cmdline" --unicode
    done
    
    clear
    efibootmgr --unicode
    echo "Finished!"
}

# script argument definitions
case $1 in
    
    -v|--version)
        version
        ;;
    
    -h|--help|"")
        helper
        ;;

    -c|--create-entries)
    create_entries
        ;;
esac

exit

