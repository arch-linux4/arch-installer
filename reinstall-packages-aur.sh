#!/bin/bash

# lists all aur packages 
paru -Qm > ./paru.list

# installs all aur packages listed in paru.lst
# avoids reinstallation of already-up-to-date packages
paru -S --needed --noconfirm $(cat ~/dotfiles/$HOSTNAME/paru.list)
