#!/usr/bin/bash

# Productivity

## Web browser
pacman --noconfirm -S firefox

## Matrix Client
pacman --noconfirm -S element-desktop

## Password manager
pacman --noconfirm -S keepassxc

## Personal finance manager
pacman --noconfirm -S gnucash gnucash-docs

## File manager
pacman --noconfirm -S thunar thunar-volman
paru --noconfirm -S papirus-folders
papirus-folder --color blue --theme Papirus-Dark
echo "TerminalEmulator=alacritty" > ~/.config/xfce4/helpers.rc

## Office document editing suite
pacman --noconfirm -S libreoffice-fresh 
paru --noconfirm -S papirus-libreoffice-theme

## PDF document viewer
pacman --noconfirm -S evince

## PDF document editing
pacman --noconfirm -S qpdf bcprov java-commons-lang

## Image viewer
pacman --noconfirm -S gthumb

## Music player
pacman --noconfirm -S cmus

## Video player
pacman --noconfirm -S mpv

## Screenshots
pacman --noconfirm -S flameshot

## Hand-written notes
pacman --noconfirm -S xournalpp

## Privacy-focused Youtube client
paru --noconfirm -S freetube-bin

## Graphics
pacman --noconfirm -S gimp inkscape krita material-maker
pacman --noconfirm -S blender natron-bin f3d
paru --noconfirm -S spacenavd

## Audio/Video editing
paru --noconfirm -S tenacity soundconverter picard
paru --noconfirm -S olive
pacman --noconfirm -S handbrake
paru --noconfirm -S makemkv makemkv-libaacs
pacman --noconfirm -S obs-studio

## Research papers management
pacman --noconfirm -S zotero

## Backup archives management
pacman --noconfirm -S kiwix-desktop kiwix-tools libkiwix

## Manga/Anime downloader utilities
mkdir -vp $HOME/build
git clone https://github.com/KevCui/animepahe-dl.git ~/build/animepahe-dl
paru --noconfirm -S mangal

# Development

## Text editor / IDE
paru --noconfirm -S lite-xl
pacman --noconfirm -S fontconfig
pacman --noconfirm neovim ripgrep

## Source control management
paru -S gitkraken github-desktop-bin

## C/C++
pacman --noconfirm -S gcc make

## Java
pacman --noconfirm -S jdk-openjdk
paru --noconfirm -S eclipse-java

## R
paru --noconfirm -S rstudio-desktop-bin

## Assembly MIPS-2000
paru --noconfirm -S qtspim

## Minecraft mods
pacman --noconfirm -S jdk8-openjdk jdk11-openjdk jdk17-openjdk
paru --noconfirm -S blockbench-bin

## Arduino
paru -S arduino-ide-bin

## PCB
pacman --noconfirm -S kicad

## CAD
pacman --noconfirm -S freecad openscad

## Chemistry
paru --noconfirm -S avogadro2

## TeX
pacman --noconfirm -S texlive-most texlive-lang texstudio

## Fonts
pacman --noconfirm -S birdfont fontforge gucharmap

## Lua
pacman --noconfirm -S lua

## Web (NPM)
pacman --noconfirm -S nodejs

## Game dev
pacman --noconfirm -S godot
paru --noconfirm -S godot3-bin

# Games

## Open source
pacman --noconfirm -S 0ad
pacman --noconfirm -S minetest
pacman --noconfirm -S supertuxkart
pacman --noconfirm -S xonotic
paru --noconfirm -S airshipper

## Minecraft
paru --noconfirm -S prismlauncher-qt5-bin

# Window manager setup utility apps

## Web browser
pacman --noconfirm -S nyxt

## Password manager
pacman --noconfirm -S pass

## File manager
pacman --noconfirm -S ranger poppler w3m nnn

## PDF document viewer
pacman --noconfirm -S mupdf

## Screenshot
pacman --noconfirm -S scrot
