#!/bin/bash

## Prepare the installation
./part1.sh

## Configure the installation
./part2.sh

## Customize the installation
./part3.sh 

## Install applications
./part4.sh

## Setup user services
./part5.sh

## Setup dotfiles
./part6.sh

## Final Reboot
./part7.sh
