#!/bin/bash

DIR=$HOME/dotfiles/$HOSTNAME/home/$USER
DIR_USR=$HOME/dotfiles/$HOSTNAME/usr
DIR_ETC=$HOME/dotfiles/$HOSTNAME/etc
DIR_LIB=$HOME/dotfiles/$HOSTNAME/lib

cp -vrp /home/raffaele/.Xresources $DIR
cp -vrp /home/raffaele/.xprofile $DIR
cp -vrp /home/raffaele/.xinitrc $DIR

cp -vrp /home/raffaele/.bashrc $DIR
cp -vrp /home/raffaele/.bash_profile $DIR

cp -vrp /usr/local/bin $DIR_USR/usr/local/bin/*.sh

cp -vrp /home/raffaele/.vim $DIR/.vim
cp -vrp /home/raffaele/.vim.plugged $DIR/.vim.plugged
cp -vrp /home/raffaele/.vimrc $DIR
cp -vrp /home/raffaele/.vimrc.plug $DIR

mkdir -vp $DIR/.config
cp -vrp /home/raffaele/.config/bspwm $DIR/.config/bspwm
cp -vrp /home/raffaele/.config/dunst $DIR/.config/dunst
cp -vrp /home/raffaele/.config/picom $DIR/.config/picom
cp -vrp /home/raffaele/.config/polybar $DIR/.config/polybar
cp -vrp /home/raffaele/.config/rofi $DIR/.config/rofi
cp -vrp /home/raffaele/.config/sxhkd $DIR/.config/sxhkd

cp -vrp /home/raffaele/wallpapers $DIR/wallpapers

cp -vrp /home/raffaele/.fonts $DIR/.fonts
cp -vrp /home/raffaele/.icons $DIR/.icons
cp -vrp /home/raffaele/.themes $DIR/.themes

dconf dump /org/cinnamon/ > $HOME/dotfiles/$HOSTNAME/home/raffaele/.config/cinnamon/settings-backup

mkdir -vp $DIR/.ssh
cp -vrp /home/raffaele/.ssh/config $DIR/.ssh/
cp -vrp /home/raffaele/.ssh/known_hosts $DIR/.ssh/

mkdir $DIR/.config/alacritty
cp -vrp /home/raffaele/.config/alacritty/alacritty.yml $DIR/.config/alacritty/alacritty.yml
cp -vrp /home/raffaele/.config/tmux $DIR/.config/tmux
cp -vrp /home/raffaele/.config/nvim $DIR/.config/nvim
