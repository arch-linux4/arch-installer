#!/bin/bash

## 1 - Introduction
dialog --defaultno --title "Arch Linux" --yesno \
"This is my personal arch linux setup. \n\n\
Do you want to continue?" 15 60 || exit

## 2 - Setup keyboard layout
loadkeys us

## 3 - Check UEFI
check_uefi=$(cat /sys/firmware/efi/fw_platform_size)
if [[ $check_uefi = "" ]]; then
  echo -e "This computer doesn't support UEFI!"
fi

## 4 - Check Internet connection

## 5 - If there is a WIFI card, choose wether or not to configure the wifi connection

## 6 - Syncronize system clock with the Network Time Protocol
timedatectl set-ntp true

## 7 - Partitioning

## 8 - Setup repository mirrors
reflector --verbose --download-timeout 10 --latest 10 --protocol https --completion-percent 100 --country Germany --sort rate --save /etc/pacman.d/mirrorlist

## 9 - Install base packages
pacstrap /mnt base base-devel linux linux-lts linux-firmware man-db man-pages texinfo vim

## 10 - Generate file system tab
genfstab -U /mnt >> /mnt/etc/fstab
