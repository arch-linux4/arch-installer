#!/usr/bin/bash

DDIR="$HOME/downloads/bspwm-setup$HOME"  # $HOME is /home/username
DCDIR="$DDIR/.config"
DLDIR="$DDIR/.local"
CDIR="$HOME/.config"
LDIR="$HOME/.local"

cd $HOME
mkdir -vp $HOME/downloads/bspwm-setup$HOME
mkdir -vp $HOME/downloads/bspwm-setup$CDIR
mkdir -vp $HOME/downloads/bspwm-setup$LDIR/bin
mkdir -vp $HOME/downloads/bspwm-setup$LDIR/share

# Copy settings

## X
x_list=(.Xresources
        .xprofile
        .xinitrc)
for i in $x_list; do
  cp -vr $HOME/$i $DDIR/$i
done

## Shell
sh_list=(.bashrc
         .bash_profile)
for i in $sh_list; do
  cp -vr $HOME/$i $DDIR/$i
done

## GTK
cp -vr $HOME/.gtkrc-2.0 $DDIR/.gtkrc-2.0
gtk_list=(gtk-2.0
          gtk-3.0
          gtk-4.0)
for i in $gtk_list; do
  cp -vr $CDIR/$i $DCDIR/$i
done

## QT
qt_list=(qt5ct
         qtcurve
         Kvantum
         lxsession)
for i in $qt_list; do
  cp -vr $CDIR/$i $DCDIR/$i
done

## Cinnamon settings
dconf dump /org/cinnamon/ > $DDIR/cinnamon-settings

## Custom scripts
cp -vr $LDIR/bin $DLDIR/

# ## Apps
cp -vr $HOME/.ssh $DDIR/.ssh
cp -vr $HOME/.fonts $DDIR/.fonts
cp -vr $HOME/.icons $DDIR/.icons
cp -vr $HOME/.themes $DDIR/.themes
cp -vr $HOME/.vimrc $DDIR/.vimrc
app_list_c=(0ad
            alacritty
            autostart
            blender
            Blockbench
            borg
            bspwm
            cinnamon
            cinnamon-session
            cmus
            corectrl
            dunst
            Element
            evince
            firejail
            flameshot
            FreeCAD
            FreeTube
            GIMP
            GitKraken
            gnucash
            godot
            gpick
            gthumb
            heroic
            htop
            inkscape
            INRIA
            itch
            keepassxc
            kicad
            Kiwix
            libreoffice
            lite-xl
            lutris
            mangal
            mpv
            neofetch
            nvim
            nyxt
            obs-studio
            obsidian
            octave
            olivevideoeditor.org
            OpenSCAD
            openttd
            picom
            plank
            polybar
            qalculate
            ranger
            rofi
            rpcs3
            supertuxkart
            sxhkd
            syncthing
            tenacity
            texstudio
            Thunar
            tmux
            unity3d
            write-xl
            xournalpp
            ytdlp-gui
            Zulip
            cinnamon-monitors.xml)
for i in $app_list_c; do
  cp -vr $CDIR/$i $DCDIR/$i
done

# app_list_l=(0ad
#             airshipper
#             applications
#             cinnamon
#             Eclipse
#             FreeCAD
#             gnucash
#             godot
#             gthumb
#             INRIA
#             kicad
#             kiwix
#             krita
#             lite-xl
#             lutris
#             material_maker
#             nvim
#             nyxt
#             octave
#             olivevideoeditor.org
#             OpenSCAD
#             openttd
#             plank
#             PrismLauncher
#             qalculate
#             ranger
#             rofi
#             supertuxkart
#             warzone2100
#             xorg
#             languages.json)
# for i in $app_list_l; do
#   cp -vr $LDIR/$i $DLDIR/$i
# done

# ## Games
# cp -vr $HOME/.xonotic $DDIR/.xonotic
# cp -vr $HOME/.springlobby $DDIR/.springlobby
# cp -vr $HOME/.steam $DDIR/.steam
# cp -vr $HOME/.games $DDIR/.games
# cp -vr $LDIR/Steam $DLDIR/Steam
