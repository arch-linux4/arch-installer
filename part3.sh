#!/bin/bash

## 1 - Setup an AUR Helper
pacman -S git
mkdir -vp ~/build/paru
git clone https://aur.archlinux.org/paru.git ~/build/paru
cd ~/build/paru
makpkg -si

## 2 - Spice up pacman
sed -i 's/#Color/Color/g' /etc/pacman.conf
sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 5/g' /etc/pacman.conf
echo -e "ILoveCandy" >> /etc/pacman.conf

## 3 - Setup the login Manager
# /

## 4 - Privileges and Authentication Management
pacman -S polkit lxsession
touch ~/.xprofile
echo "lxsession &" > ~/.xprofile

## 5 - Setup a desktop environment
pacman -S cinnamon
paru -S mint-themes

## 6 - Setup a window manager
pacman -S bspwm sxhkd rofi polybar feh dunst picom i3lock imagemagick scrot python-pywal

## 7 - X configuration

## 8 - GTK and QT configuration

## 9 - Ryzen 5 1600 sudden-crash/freeze work-around

## 10 - Laptop configuration (optional)
